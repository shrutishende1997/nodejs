const express = require('express');
const bodyParser = require('body-parser');
const student = require("./Routes/student.route");
const { default: mongoose } = require('mongoose');
//const MongoClient = require("mongodb")
require("dotenv").config();
// initialize our express app
const app = express();
const URL = process.env.URL;
let port = process.env.PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/students',student);

mongoose.connect(URL).then(()=> console.log("connection succesfully..."))
.catch((err) => console.log(err));
  

app.listen(port, ()=>{
    console.log("server is up "+port);
})




