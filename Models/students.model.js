const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StudentSchema = new Schema({
    name: {type: String, required: true, max: 100},
    age: {type: Number, required: true},
    section: {type: String, required: true, max:2},
});


module.exports = mongoose.model('student_detail', StudentSchema);

//student_detail  table name
//collection name : name,age,section