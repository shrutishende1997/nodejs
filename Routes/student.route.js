const  studentController  = require('../Controller/student.controller');
const express =  require('express');
const router = express.Router();

router.get('/test',studentController.test);
router.post('/create',studentController.student_create);
router.get('/:id',studentController.student_detail);
router.put('/:id/update',studentController.student_detail_update)
router.delete('/:id/delete',studentController.student_detail_delete)


module.exports = router;






